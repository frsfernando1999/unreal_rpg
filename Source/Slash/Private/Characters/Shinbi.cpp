#include "Characters/Shinbi.h"

#include "Camera/CameraComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "EnhancedInputComponent.h"
#include "EnhancedInputSubsystems.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Items/Item.h"
#include "Items/Weapons/Weapon.h"
#include "Kismet/KismetMathLibrary.h"

AShinbi::AShinbi()
{
	PrimaryActorTick.bCanEverTick = true;

	Camera = CreateDefaultSubobject<UCameraComponent>("Camera");
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>("Camera Boom");

	CameraBoom->SetupAttachment(RootComponent);
	Camera->SetupAttachment(CameraBoom);

	GetCharacterMovement()->bOrientRotationToMovement = true;

	bUseControllerRotationYaw = false;
	bUseControllerRotationPitch = false;
	bUseControllerRotationRoll = false;
}

void AShinbi::BeginPlay()
{
	Super::BeginPlay();
	if (const APlayerController* PlayerController = Cast<APlayerController>(GetController()))
	{
		if (UEnhancedInputLocalPlayerSubsystem* InputSubsystem = ULocalPlayer::GetSubsystem<
			UEnhancedInputLocalPlayerSubsystem>(PlayerController->GetLocalPlayer()))
		{
			if (InputMapping)
			{
				InputSubsystem->AddMappingContext(InputMapping, 0);
			}
		}
	}
}

void AShinbi::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void AShinbi::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	if (UEnhancedInputComponent* EnhancedInputComponent = Cast<UEnhancedInputComponent>(PlayerInputComponent))
	{
		EnhancedInputComponent->BindAction(MoveAction, ETriggerEvent::Triggered, this, &AShinbi::Move);
		EnhancedInputComponent->BindAction(RotateCameraAction, ETriggerEvent::Triggered, this, &AShinbi::RotateCamera);
		EnhancedInputComponent->BindAction(JumpAction, ETriggerEvent::Triggered, this, &AShinbi::Jump);
		EnhancedInputComponent->BindAction(InteractAction, ETriggerEvent::Triggered, this, &AShinbi::Interact);
		EnhancedInputComponent->BindAction(AttackAction, ETriggerEvent::Triggered, this, &AShinbi::Attack);
	}
}

void AShinbi::Move(const FInputActionValue& Value)
{
	if (ActionState == EActionState::EAS_Attacking ||
	ActionState == EActionState::EAS_TogglingWeapon) return;
	const FVector2D CurrentValue = Value.Get<FVector2D>();
	if (Controller && (CurrentValue.X != 0 || CurrentValue.Y != 0))
	{
		const FRotator ControlRotation = GetControlRotation();

		const FRotator YawRotation(0.f, ControlRotation.Yaw, 0.f);
		const FVector Forward = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);
		AddMovementInput(Forward, CurrentValue.Y);

		const FVector Right = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);
		AddMovementInput(Right, CurrentValue.X);
	}
}

void AShinbi::RotateCamera(const FInputActionValue& Value)
{
	const FVector2D CurrentValue = Value.Get<FVector2D>();
	if (Controller && (CurrentValue.X != 0 || CurrentValue.Y != 0))
	{
		AddControllerPitchInput(CurrentValue.Y);
		AddControllerYawInput(CurrentValue.X);
	}
}

void AShinbi::Jump()
{
	if (ActionState == EActionState::EAS_Attacking) return;
	Super::Jump();
}

void AShinbi::Interact()
{
	if (OverlappingItem)
	{
		OverlappingItem->Pickup();
		if (AWeapon* Weapon = Cast<AWeapon>(OverlappingItem))
		{
			Weapon->AttachToCharacter(this, HandSocket);
			EquipWeapon(Weapon);
		}
		OverlappingItem = nullptr;
	}
	else
	{
		ToggleEquippedWeapon();
	}
}

void AShinbi::ToggleEquippedWeapon()
{
	if (EquipMontage == nullptr ||
		EquippedWeapon == nullptr ||
		ActionState != EActionState::EAS_Unoccupied
	)
		return;

	UAnimInstance* AnimInstance = GetMesh()->GetAnimInstance();
	if (AnimInstance == nullptr) return;

	ActionState = EActionState::EAS_TogglingWeapon;
	if (CharacterState == ECharacterState::ECS_Unequipped)
	{
		AnimInstance->Montage_Play(EquipMontage);
		AnimInstance->Montage_JumpToSection("Equip");
	}
	else if (CharacterState == ECharacterState::ECS_Equipped)
	{
		AnimInstance->Montage_Play(EquipMontage);
		AnimInstance->Montage_JumpToSection("Unequip");
	}
}


void AShinbi::GrabSword()
{
	if (CharacterState == ECharacterState::ECS_Unequipped)
	{
		EquippedWeapon->AttachToCharacter(this, HandSocket);
		CharacterState = ECharacterState::ECS_Equipped;

	}
	else if (CharacterState == ECharacterState::ECS_Equipped)
	{
		EquippedWeapon->AttachToCharacter(this, WeaponBackSocket);
		CharacterState = ECharacterState::ECS_Unequipped;
	}
}

void AShinbi::ToggleWeaponCollision(const bool bToggle)
{
	EquippedWeapon->ToggleCollision(bToggle);
}


void AShinbi::EquipWeapon(AWeapon* Weapon)
{
	EquippedWeapon = Weapon;
	CharacterState = ECharacterState::ECS_Equipped;
}

void AShinbi::Attack()
{
	if (ActionState != EActionState::EAS_Unoccupied ||
		CharacterState == ECharacterState::ECS_Unequipped ||
		GetMovementComponent() == nullptr ||
		GetMovementComponent()->IsFalling()
	)
		return;
	PlayAttackMontage();
	ActionState = EActionState::EAS_Attacking;
}

void AShinbi::SetUnoccupied()
{
	ActionState = EActionState::EAS_Unoccupied;
}


void AShinbi::PlayAttackMontage() const
{
	UAnimInstance* AnimInstance = GetMesh()->GetAnimInstance();
	if (AttackMontage && AnimInstance)
	{
		AnimInstance->Montage_Play(AttackMontage, 1);
		AnimInstance->Montage_JumpToSection(GetAttackSection(), AttackMontage);
	}
}

FName AShinbi::GetAttackSection() const
{
	switch (UKismetMathLibrary::RandomInteger(3))
	{
	case 0:
		return FName("Attack1");
	case 1:
		return FName("Attack2");
	case 2:
		return FName("Attack3");
	default:
		return FName("Attack1");
	}
}
