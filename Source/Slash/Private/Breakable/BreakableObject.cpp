#include "Breakable/BreakableObject.h"

#include "GeometryCollection/GeometryCollectionComponent.h"
#include "Items/Treasure.h"

ABreakableObject::ABreakableObject()
{
	PrimaryActorTick.bCanEverTick = true;
	GeometryCollection = CreateDefaultSubobject<UGeometryCollectionComponent>("Geometry Collection");
	SetRootComponent(GeometryCollection);

	GeometryCollection->SetGenerateOverlapEvents(true);
	GeometryCollection->SetCollisionResponseToChannel(ECC_Pawn, ECR_Ignore);
	GeometryCollection->SetCollisionResponseToChannel(ECC_Camera, ECR_Ignore);
	GeometryCollection->bNotifyBreaks = true;

}

void ABreakableObject::BeginPlay()
{
	Super::BeginPlay();
}

void ABreakableObject::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ABreakableObject::GetHit_Implementation(const FVector& HitLocation)
{
	if (GetWorld() && TreasureClasses.Num() > 0)
	{
		FVector Location = GetActorLocation();
		Location.Z += 50.f;
		GetWorld()->SpawnActor<ATreasure>(TreasureClasses[FMath::RandRange(0, TreasureClasses.Num() - 1)], Location, GetActorRotation());
	}
}
