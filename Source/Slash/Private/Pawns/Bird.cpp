#include "Pawns/Bird.h"

#include "Components/CapsuleComponent.h"
#include "EnhancedInputSubsystems.h"
#include "EnhancedInputComponent.h"
#include "Camera/CameraComponent.h"
#include "GameFramework/FloatingPawnMovement.h"
#include "GameFramework/SpringArmComponent.h"


ABird::ABird()
{
	PrimaryActorTick.bCanEverTick = true;
	BirdMesh = CreateDefaultSubobject<USkeletalMeshComponent>("Mesh");
	Capsule = CreateDefaultSubobject<UCapsuleComponent>("Capsule Component");
	Camera = CreateDefaultSubobject<UCameraComponent>("Camera");
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>("Camera Boom");
	FloatingPawn = CreateDefaultSubobject<UFloatingPawnMovement>("Floating Pawn Component");

	SetRootComponent(Capsule);
	BirdMesh->SetupAttachment(Capsule);
	CameraBoom->SetupAttachment(Capsule);
	Camera->SetupAttachment(CameraBoom);

	AutoPossessPlayer = EAutoReceiveInput::Player0;
	bUseControllerRotationYaw = true;
	bUseControllerRotationPitch = true;
}

void ABird::BeginPlay()
{
	Super::BeginPlay();
	if (const APlayerController* PlayerController = Cast<APlayerController>(GetController()))
	{
		if (UEnhancedInputLocalPlayerSubsystem* InputSubsystem = ULocalPlayer::GetSubsystem<
			UEnhancedInputLocalPlayerSubsystem>(PlayerController->GetLocalPlayer()))
		{
			if (InputMapping)
			{
				InputSubsystem->AddMappingContext(InputMapping, 0);
			}
		}
	}
}

void ABird::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void ABird::Move(const FInputActionValue& Value)
{
	const float CurrentValue = Value.Get<float>();
	if (Controller && CurrentValue != 0)
	{
		AddMovementInput(GetActorForwardVector(), CurrentValue);
	}
}

void ABird::RotateCamera(const FInputActionValue& Value)
{
	const FVector2D CurrentValue = Value.Get<FVector2D>();
	if (Controller && (CurrentValue.X != 0 || CurrentValue.Y != 0))
	{
		AddControllerYawInput(CurrentValue.X);
		AddControllerPitchInput(CurrentValue.Y);
	}
}

void ABird::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	if (UEnhancedInputComponent* EnhancedInputComponent = Cast<UEnhancedInputComponent>(PlayerInputComponent))
	{
		EnhancedInputComponent->BindAction(MoveAction, ETriggerEvent::Triggered, this, &ABird::Move);
		EnhancedInputComponent->BindAction(RotateCameraAction, ETriggerEvent::Triggered, this, &ABird::RotateCamera);
	}
}
