#include "Items/Item.h"

#include "Characters/Shinbi.h"
#include "Components/SphereComponent.h"

AItem::AItem()
{
	PrimaryActorTick.bCanEverTick = true;

	ItemMesh = CreateDefaultSubobject<UStaticMeshComponent>("Item Mesh");
	Sphere = CreateDefaultSubobject<USphereComponent>("Sphere");

	ItemMesh->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	
	SetRootComponent(ItemMesh);
	Sphere->SetupAttachment(ItemMesh);
	Sphere->SetSphereRadius(120.f);
}

void AItem::BeginPlay()
{
	Super::BeginPlay();
	Location = GetActorLocation();
	Sphere->OnComponentBeginOverlap.AddDynamic(this, &AItem::OnSphereOverlap);
	Sphere->OnComponentEndOverlap.AddDynamic(this, &AItem::OnSphereEndOverlap);
}

void AItem::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	RunningTime += DeltaTime;
	if (ItemState == EItemState::EIS_Dropped) AddRotationAndFloatingCurve(DeltaTime);
}


void AItem::OnSphereOverlap(UPrimitiveComponent* OverlappedComponent,
                            AActor* OtherActor,
                            UPrimitiveComponent* OtherComp,
                            int32 OtherBodyIndex,
                            bool bFromSweep,
                            const FHitResult& SweepResult)
{
	if (AShinbi* Shinbi = Cast<AShinbi>(OtherActor))
	{
		Shinbi->SetOverlappingItem(this);
	}
}

void AItem::OnSphereEndOverlap(UPrimitiveComponent* OverlappedComponent,
                               AActor* OtherActor,
                               UPrimitiveComponent* OtherComp,
                               int32 OtherBodyIndex)
{
	if (AShinbi* Shinbi = Cast<AShinbi>(OtherActor))
	{
		Shinbi->SetOverlappingItem(nullptr);
	}
}


void AItem::AddRotationAndFloatingCurve(float DeltaTime)
{
	AddActorWorldOffset(FVector(0.f, 0.f, TransformSin()));
	AddActorWorldRotation(FRotator(0.f, RotationSpeed * DeltaTime, 0.f));
}


float AItem::TransformSin() const
{
	return Amplitude * FMath::Sin(RunningTime * TimeConstant);
}

float AItem::TransformCos() const
{
	return Amplitude * FMath::Cos(RunningTime * TimeConstant);
}

void AItem::Pickup()
{
		Sphere->SetGenerateOverlapEvents(false);
		ItemState = EItemState::EIS_PickedUp;
}
