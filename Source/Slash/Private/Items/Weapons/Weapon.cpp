#include "Items/Weapons/Weapon.h"

#include "Components/BoxComponent.h"
#include "GameFramework/Character.h"
#include "Interfaces/HitInterface.h"
#include "Kismet/KismetSystemLibrary.h"


AWeapon::AWeapon()
{
	WeaponCollisionBox = CreateDefaultSubobject<UBoxComponent>("Weapon Box");
	WeaponCollisionBox->SetupAttachment(GetRootComponent());
	WeaponCollisionBox->SetCollisionEnabled(ECollisionEnabled::NoCollision);

	StartTrace = CreateDefaultSubobject<USceneComponent>("Start Trace");
	StartTrace->SetupAttachment(GetRootComponent());

	EndTrace = CreateDefaultSubobject<USceneComponent>("End Trace");
	EndTrace->SetupAttachment(GetRootComponent());
}

void AWeapon::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);
}

void AWeapon::BeginPlay()
{
	Super::BeginPlay();
	WeaponCollisionBox->OnComponentBeginOverlap.AddDynamic(this, &AWeapon::BoxOverlap);
}


void AWeapon::AttachToCharacter(ACharacter* InParent, const FString& WeaponSocket)
{
	OwningCharacter = InParent;
	ItemMesh->AttachToComponent(InParent->GetMesh(), FAttachmentTransformRules::SnapToTargetIncludingScale,
	                            FName(WeaponSocket));
}

void AWeapon::Pickup()
{
	Super::Pickup();
	ItemState = EItemState::EIS_Equipped;
}

void AWeapon::ToggleCollision(const bool bToggle)
{
	if (bToggle)
	{
		if (OwningCharacter) IgnoredActors.Add(OwningCharacter);
		WeaponCollisionBox->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	}
	else
	{
		WeaponCollisionBox->SetCollisionEnabled(ECollisionEnabled::NoCollision);
		IgnoredActors.Empty();
	}
}


void AWeapon::OnSphereOverlap(UPrimitiveComponent* OverlappedComponent,
                              AActor* OtherActor,
                              UPrimitiveComponent* OtherComp,
                              int32 OtherBodyIndex,
                              bool bFromSweep,
                              const FHitResult& SweepResult)
{
	Super::OnSphereOverlap(OverlappedComponent, OtherActor, OtherComp, OtherBodyIndex, bFromSweep, SweepResult);
}

void AWeapon::OnSphereEndOverlap(UPrimitiveComponent* OverlappedComponent,
                                 AActor* OtherActor,
                                 UPrimitiveComponent* OtherComp,
                                 int32 OtherBodyIndex)
{
	Super::OnSphereEndOverlap(OverlappedComponent, OtherActor, OtherComp, OtherBodyIndex);
}

void AWeapon::BoxOverlap(UPrimitiveComponent* OverlappedComponent,
                         AActor* OtherActor,
                         UPrimitiveComponent* OtherComp,
                         int32 OtherBodyIndex,
                         bool bFromSweep,
                         const FHitResult& SweepResult)
{
	FHitResult HitResult;

	UKismetSystemLibrary::BoxTraceSingle(
		this,
		StartTrace->GetComponentLocation(),
		EndTrace->GetComponentLocation(),
		FVector(2.5f, 2.5f, 2.5f),
		StartTrace->GetComponentRotation(),
		UEngineTypes::ConvertToTraceType(ECC_Visibility),
		false,
		IgnoredActors,
		EDrawDebugTrace::None,
		HitResult,
		true);
	
	if (IHitInterface* HittableActor = Cast<IHitInterface>(HitResult.GetActor()))
	{
		IgnoredActors.AddUnique(HitResult.GetActor());
		HittableActor->Execute_GetHit(HitResult.GetActor(), HitResult.ImpactPoint);
		CreateTranscientFields(HitResult.ImpactPoint);
	}
}
