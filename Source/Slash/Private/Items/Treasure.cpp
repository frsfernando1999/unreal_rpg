#include "Items/Treasure.h"

#include "Characters/Shinbi.h"
#include "Kismet/GameplayStatics.h"

ATreasure::ATreasure()
{
}

void ATreasure::BeginPlay()
{
	Super::BeginPlay();
}

void ATreasure::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);
}

void ATreasure::OnSphereOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
                                UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep,
                                const FHitResult& SweepResult)
{
	if (AShinbi* Shinbi = Cast<AShinbi>(OtherActor))
	{
		if (PickupSound)
		{
			UGameplayStatics::PlaySoundAtLocation(this, PickupSound, GetActorLocation());
		}
		Destroy();
	}
}

void ATreasure::OnSphereEndOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
                                   UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	if (AShinbi* Shinbi = Cast<AShinbi>(OtherActor))
	{
	}
}
