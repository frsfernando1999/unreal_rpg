#include "AnimInstance/ShinbiAnimInstance.h"

#include "Characters/Shinbi.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Kismet/KismetMathLibrary.h"

void UShinbiAnimInstance::NativeInitializeAnimation()
{
	Super::NativeInitializeAnimation();
	Shinbi = Cast<AShinbi>(TryGetPawnOwner());
	if (Shinbi)
	{
		MovementComponent = Shinbi->GetCharacterMovement();
	}
}

void UShinbiAnimInstance::NativeUpdateAnimation(float DeltaSeconds)	
{
	Super::NativeUpdateAnimation(DeltaSeconds);
	if (!MovementComponent || !Shinbi) return;
	GroundSpeed = UKismetMathLibrary::VSizeXY(MovementComponent->Velocity);
	bIsFalling = MovementComponent->IsFalling();
	CharacterState = Shinbi->GetCharacterState();
}

void UShinbiAnimInstance::SetUnoccupied()
{
	if (Shinbi)
	{
		Shinbi->SetUnoccupied();
	}
}

void UShinbiAnimInstance::GrabSword()
{
	if (Shinbi)
	{
		Shinbi->GrabSword();
	}
}

void UShinbiAnimInstance::TurnOnWeaponCollision()
{
	if (Shinbi)
	{
		Shinbi->ToggleWeaponCollision(true);
	}
}

void UShinbiAnimInstance::TurnOffWeaponCollision()
{
	if (Shinbi)
	{
		Shinbi->ToggleWeaponCollision(false);
	}
}


