#pragma once

#include "CoreMinimal.h"
#include "Animation/AnimInstance.h"
#include "Enums/CharacterState.h"
#include "ShinbiAnimInstance.generated.h"

UCLASS()
class SLASH_API UShinbiAnimInstance : public UAnimInstance
{
	GENERATED_BODY()

public:
	virtual void NativeInitializeAnimation() override;
	virtual void NativeUpdateAnimation(float DeltaSeconds) override;

	UFUNCTION(BlueprintCallable)
	void SetUnoccupied();

	UFUNCTION(BlueprintCallable)
	void GrabSword();

	UFUNCTION(BlueprintCallable)
	void TurnOnWeaponCollision();

	UFUNCTION(BlueprintCallable)
	void TurnOffWeaponCollision();

protected:
	TObjectPtr<class AShinbi> Shinbi;
	TObjectPtr<class UCharacterMovementComponent> MovementComponent;
	
	UPROPERTY(BlueprintReadOnly)
	float GroundSpeed;
	
	UPROPERTY(BlueprintReadOnly)
	bool bIsFalling;

	UPROPERTY(BlueprintReadOnly)
	ECharacterState CharacterState;
};
