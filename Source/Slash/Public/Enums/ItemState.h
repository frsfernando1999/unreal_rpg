﻿#pragma once

UENUM(BlueprintType)
enum class EItemState : uint8
{
	EIS_Dropped UMETA(DisplayName = "Dropped"),
	EIS_PickedUp UMETA(DisplayName = "Picked Up"),
	EIS_Equipped UMETA(DisplayName = "Equipped"),
	EIS_Default_Max UMETA(DisplayName = "Default Max")
};