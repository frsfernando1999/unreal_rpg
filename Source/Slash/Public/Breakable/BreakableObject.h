#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Interfaces/HitInterface.h"
#include "BreakableObject.generated.h"

UCLASS()
class SLASH_API ABreakableObject : public AActor, public IHitInterface
{
	GENERATED_BODY()

public:	
	ABreakableObject();
	virtual void Tick(float DeltaTime) override;
	
	virtual void GetHit_Implementation(const FVector& HitLocation) override;

protected:
	
	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TObjectPtr<class UGeometryCollectionComponent> GeometryCollection;

private:
	UPROPERTY(EditAnywhere)
	TArray<TSubclassOf <class ATreasure>> TreasureClasses;
};
