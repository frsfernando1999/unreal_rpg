#pragma once

#include "CoreMinimal.h"
#include "Enums/ItemState.h"
#include "GameFramework/Actor.h"
#include "Item.generated.h"

UCLASS()
class SLASH_API AItem : public AActor
{
	GENERATED_BODY()

public:
	AItem();
	virtual void Tick(float DeltaTime) override;
	virtual void Pickup();

protected:
	virtual void BeginPlay() override;

	UFUNCTION(BlueprintPure)
	float TransformSin() const;
	UFUNCTION(BlueprintPure)
	float TransformCos() const;

	UFUNCTION()
	virtual void OnSphereOverlap(UPrimitiveComponent* OverlappedComponent,
	                     AActor* OtherActor,
	                     UPrimitiveComponent* OtherComp,
	                     int32 OtherBodyIndex,
	                     bool bFromSweep,
	                     const FHitResult& SweepResult);

	UFUNCTION()
	virtual void OnSphereEndOverlap(UPrimitiveComponent* OverlappedComponent,
	                        AActor* OtherActor,
	                        UPrimitiveComponent* OtherComp,
	                        int32 OtherBodyIndex);

	UPROPERTY(VisibleAnywhere)
	TObjectPtr<UStaticMeshComponent> ItemMesh;

	UPROPERTY(VisibleAnywhere)
	TObjectPtr<class USphereComponent> Sphere;

	EItemState ItemState = EItemState::EIS_Dropped;

private:
	
	void AddRotationAndFloatingCurve(float DeltaTime);
	
	UPROPERTY(EditAnywhere)
	float Amplitude = 0.1f;
	
	UPROPERTY(EditAnywhere)
	float TimeConstant = 2.f;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, meta=(AllowPrivateAccess = "true"))
	float RotationSpeed = 45.f;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta=(AllowPrivateAccess = "true"))
	float RunningTime = 0.f;
	
	FVector Location;
};
