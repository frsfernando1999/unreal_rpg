#pragma once

#include "CoreMinimal.h"
#include "Items/Item.h"
#include "Weapon.generated.h"

UCLASS()
class SLASH_API AWeapon : public AItem
{
	GENERATED_BODY()

public:
	AWeapon();
	virtual void Tick(float DeltaSeconds) override;
	void AttachToCharacter(ACharacter* InParent, const FString& WeaponSocket);
	virtual void Pickup() override;
	void ToggleCollision(bool bToggle);

protected:
	UFUNCTION()
	virtual void BeginPlay() override;

	virtual void OnSphereOverlap(UPrimitiveComponent* OverlappedComponent,
	                             AActor* OtherActor,
	                             UPrimitiveComponent* OtherComp,
	                             int32 OtherBodyIndex,
	                             bool bFromSweep,
	                             const FHitResult& SweepResult) override;
	virtual void OnSphereEndOverlap(UPrimitiveComponent* OverlappedComponent,
	                                AActor* OtherActor,
	                                UPrimitiveComponent* OtherComp,
	                                int32 OtherBodyIndex) override;
	UFUNCTION()
	void BoxOverlap(UPrimitiveComponent* OverlappedComponent,
	                AActor* OtherActor,
	                UPrimitiveComponent* OtherComp,
	                int32 OtherBodyIndex,
	                bool bFromSweep,
	                const FHitResult& SweepResult);

	UFUNCTION(BlueprintImplementableEvent)
	void CreateTranscientFields(const FVector& FieldLocation);

	UPROPERTY(VisibleAnywhere, Category="Weapon Properties")
	TObjectPtr<class UBoxComponent> WeaponCollisionBox;

	UPROPERTY(VisibleAnywhere, Category="Weapon Properties")
	TObjectPtr<USceneComponent> StartTrace;

	UPROPERTY(VisibleAnywhere, Category="Weapon Properties")
	TObjectPtr<USceneComponent> EndTrace;

	UPROPERTY(VisibleAnywhere, Category="Weapon Properties")
	TObjectPtr<ACharacter> OwningCharacter;

private:
	UPROPERTY()
	TArray<AActor*> IgnoredActors;

public:
	FORCEINLINE TObjectPtr<UStaticMeshComponent> GetMesh() const { return ItemMesh; }
};
