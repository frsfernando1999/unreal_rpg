#pragma once

#include "CoreMinimal.h"
#include "Enums/ActionState.h"
#include "Enums/CharacterState.h"
#include "GameFramework/Character.h"
#include "Shinbi.generated.h"

UCLASS()
class SLASH_API AShinbi : public ACharacter
{
	GENERATED_BODY()

public:
	AShinbi();
	virtual void Tick(float DeltaTime) override;
	virtual void SetupPlayerInputComponent(UInputComponent* PlayerInputComponent) override;

	void EquipWeapon(class AWeapon* Weapon);
	void SetUnoccupied();
	void GrabSword();
	void ToggleWeaponCollision(bool bToggle);

protected:
	virtual void BeginPlay() override;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	ECharacterState CharacterState = ECharacterState::ECS_Unequipped;
	EActionState ActionState = EActionState::EAS_Unoccupied;

private:
	void Attack();
	void Move(const struct FInputActionValue& Value);
	void RotateCamera(const FInputActionValue& Value);
	virtual void Jump() override;
	void ToggleEquippedWeapon();
	void Interact();
	void PlayAttackMontage() const;
	FName GetAttackSection() const;


	UPROPERTY(VisibleAnywhere)
	TObjectPtr<class UCameraComponent> Camera;

	UPROPERTY(VisibleAnywhere)
	TObjectPtr<class USpringArmComponent> CameraBoom;

	UPROPERTY(EditAnywhere, Category="Input")
	TObjectPtr<class UInputMappingContext> InputMapping;

	UPROPERTY(EditAnywhere, Category="Input")
	TObjectPtr<class UInputAction> MoveAction;

	UPROPERTY(EditAnywhere, Category="Input")
	TObjectPtr<UInputAction> RotateCameraAction;

	UPROPERTY(EditAnywhere, Category="Input")
	TObjectPtr<UInputAction> JumpAction;

	UPROPERTY(EditAnywhere, Category="Input")
	TObjectPtr<UInputAction> InteractAction;

	UPROPERTY(EditAnywhere, Category="Input")
	TObjectPtr<UInputAction> AttackAction;

	UPROPERTY(VisibleAnywhere)
	TObjectPtr<class AItem> OverlappingItem;

	UPROPERTY(VisibleAnywhere)
	TObjectPtr<AWeapon> EquippedWeapon;

	UPROPERTY(EditDefaultsOnly, Category="Animation")
	TObjectPtr<UAnimMontage> AttackMontage;

	UPROPERTY(EditDefaultsOnly, Category="Animation")
	TObjectPtr<UAnimMontage> EquipMontage;

	UPROPERTY(EditAnywhere)
	FString HandSocket = FString("right_hand_socket");

	UPROPERTY(EditAnywhere)
	FString WeaponBackSocket = FString("weapon_back_socket");


public:
	FORCEINLINE void SetOverlappingItem(AItem* Item) { OverlappingItem = Item; }
	FORCEINLINE AWeapon* GetEquippedItem() const { return EquippedWeapon; }
	FORCEINLINE ECharacterState GetCharacterState() const { return CharacterState; }
};
